import { Module } from '@nestjs/common';
import { CommonModule } from './common/common.module';
import { TwilioModule } from './twilio/twilio.module';

@Module({
  imports: [CommonModule, TwilioModule],
})
export class AppModule {}
