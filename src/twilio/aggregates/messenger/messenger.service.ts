import { Injectable, Inject, Logger } from '@nestjs/common';
import { Twilio } from 'twilio';
import { TWILIO_CONNECTION } from '../../twilio.provider';
import { ConfigService, TWILIO_NUMBER } from '../../../common/config.service';
import { UnverifiedPhoneAddedPayload } from '../../controllers/message/unverified-phone-added.interface';
import { UserLogInHOTPGenerated } from '../../controllers/message/login-hotp-generated.interface';

@Injectable()
export class MessengerService {
  constructor(
    @Inject(TWILIO_CONNECTION)
    private readonly client: Twilio,
    private readonly config: ConfigService,
  ) {}

  async sendMessage(body: string, to: string) {
    const from = this.config.get(TWILIO_NUMBER);
    return await this.client.messages.create({ body, to, from });
  }

  async sendPhoneVerificationMessage(payload: UnverifiedPhoneAddedPayload) {
    const { eventData } = payload;
    if (eventData && eventData.hotp) {
      const { phoneOTP } = eventData;
      if (phoneOTP && phoneOTP.metaData && phoneOTP.metaData.phone) {
        let message = `Phone verification OTP ${eventData.hotp}.`;
        message += `Code expires on ${phoneOTP.expiry}. Do not share with anyone.`;
        try {
          await this.sendMessage(message, phoneOTP.metaData.phone);
        } catch (error) {
          Logger.error(error, error?.toString(), this.constructor.name);
        }
      }
    }
  }

  async sendLoginOTPtoPhone(payload: UserLogInHOTPGenerated) {
    const { eventData } = payload;
    if (eventData && eventData.hotp) {
      const { user } = eventData;
      if (user && user.phone) {
        const message = `Login OTP ${eventData.hotp}. Do not share with anyone.`;
        try {
          await this.sendMessage(message, user.phone);
        } catch (error) {
          Logger.error(error, error?.toString(), this.constructor.name);
        }
      }
    }
  }
}
