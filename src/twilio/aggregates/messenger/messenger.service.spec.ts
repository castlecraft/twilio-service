import { Test, TestingModule } from '@nestjs/testing';
import { MessengerService } from './messenger.service';
import { TWILIO_CONNECTION } from '../../twilio.provider';
import { ConfigService } from '../../../common/config.service';

describe('MessengerService', () => {
  let service: MessengerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MessengerService,
        { provide: TWILIO_CONNECTION, useValue: {} },
        { provide: ConfigService, useValue: {} },
      ],
    }).compile();

    service = module.get<MessengerService>(MessengerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
