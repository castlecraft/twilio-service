import * as twilio from 'twilio';
import {
  ConfigService,
  TWILIO_SID,
  TWILIO_AUTH_TOKEN,
} from '../common/config.service';

export const TWILIO_CONNECTION = 'TWILIO_CONNECTION';

export const TwilioProvider = [
  {
    provide: TWILIO_CONNECTION,
    useFactory: (config: ConfigService) => {
      return twilio(config.get(TWILIO_SID), config.get(TWILIO_AUTH_TOKEN));
    },
    inject: [ConfigService],
  },
];
