export interface User {
  disabled: boolean;
  roles: string[];
  enable2fa: boolean;
  otpPeriod: number;
  deleted: boolean;
  enablePasswordLess: boolean;
  isEmailVerified: boolean;
  name: string;
  creation: Date;
  uuid: string;
  phone: string;
}

export interface EventData {
  user: User;
  hotp: string;
}

export interface UserLogInHOTPGenerated {
  eventId: string;
  eventName: string;
  eventFromService: string;
  eventDateTime: Date;
  eventData: EventData;
}
