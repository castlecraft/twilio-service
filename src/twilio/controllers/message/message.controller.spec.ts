import { Test, TestingModule } from '@nestjs/testing';
import { MessageController } from './message.controller';
import { MessengerService } from '../../aggregates/messenger/messenger.service';

describe('Message Controller', () => {
  let controller: MessageController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MessageController],
      providers: [{ provide: MessengerService, useValue: {} }],
    }).compile();

    controller = module.get<MessageController>(MessageController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
