export interface User {
  disabled: boolean;
  roles: string[];
  enable2fa: boolean;
  otpPeriod: number;
  deleted: boolean;
  enablePasswordLess: boolean;
  isEmailVerified: boolean;
  _id: string;
  name: string;
  creation: Date;
  uuid: string;
  phone: string;
}

export interface MetaData {
  counter: number;
  secret: string;
  phone: string;
}

export interface PhoneOTP {
  _id: string;
  metaData: MetaData;
  entity: string;
  entityUuid: string;
  authDataType: string;
  expiry: Date;
  uuid: string;
}

export interface EventData {
  user: User;
  phoneOTP: PhoneOTP;
  hotp: string;
}

export interface UnverifiedPhoneAddedPayload {
  eventId: string;
  eventName: string;
  eventFromService: string;
  eventDateTime: Date;
  eventData: EventData;
}
