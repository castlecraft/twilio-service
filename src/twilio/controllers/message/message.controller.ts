import { Controller } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { MessengerService } from '../../aggregates/messenger/messenger.service';

export const UnverifiedPhoneAddedEvent = 'UnverifiedPhoneAddedEvent';
export const UserLogInHOTPGeneratedEvent = 'UserLogInHOTPGeneratedEvent';

@Controller('twilio_message')
export class MessageController {
  constructor(private readonly messenger: MessengerService) {}

  @EventPattern(UnverifiedPhoneAddedEvent)
  async sendPhoneVerificationMessage(payload) {
    await this.messenger.sendPhoneVerificationMessage(payload);
  }

  @EventPattern(UserLogInHOTPGeneratedEvent)
  async sendLoginOTPtoPhone(payload) {
    await this.messenger.sendLoginOTPtoPhone(payload);
  }
}
