import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { setupEvents } from './events-server';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  setupEvents(app);
}
bootstrap();
