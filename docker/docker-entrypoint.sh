#!/bin/bash

function checkEnv() {
  if [[ -z "$EVENTS_PROTO" ]]; then
    echo "EVENTS_PROTO is not set"
    exit 1
  fi
  if [[ -z "$EVENTS_HOST" ]]; then
    echo "EVENTS_HOST is not set"
    exit 1
  fi
  if [[ -z "$EVENTS_PORT" ]]; then
    echo "EVENTS_PORT is not set"
    exit 1
  fi
  if [[ -z "$EVENTS_USER" ]]; then
    echo "EVENTS_USER is not set"
    exit 1
  fi
  if [[ -z "$EVENTS_PASSWORD" ]]; then
    echo "EVENTS_PASSWORD is not set"
    exit 1
  fi
  if [[ -z "$TWILIO_SID" ]]; then
    echo "TWILIO_SID is not set"
    exit 1
  fi
  if [[ -z "$TWILIO_AUTH_TOKEN" ]]; then
    echo "TWILIO_AUTH_TOKEN is not set"
    exit 1
  fi
  if [[ -z "$TWILIO_NUMBER" ]]; then
    echo "TWILIO_NUMBER is not set"
    exit 1
  fi
  if [[ -z "$NODE_ENV" ]]; then
    export NODE_ENV=production
  fi
}

function checkConnection() {
  # Wait for services
  if [[ ! -z "$EVENTS_HOST" ]] && [[ ! -z "$EVENTS_PORT" ]]; then
    echo "Connect Events . . ."
    timeout 10 bash -c 'until printf "" 2>>/dev/null >>/dev/tcp/$0/$1; do sleep 1; done' $EVENTS_HOST $EVENTS_PORT
  fi
}

function configureServer() {
  if [ ! -f .env ]; then
    envsubst '${NODE_ENV}
      ${EVENTS_HOST}
      ${EVENTS_USER}
      ${EVENTS_PASSWORD}
      ${EVENTS_PORT}
      ${EVENTS_PROTO}
      ${TWILIO_SID}
      ${TWILIO_AUTH_TOKEN}
      ${TWILIO_NUMBER}' \
      < docker/env.tmpl > .env
  fi
}
export -f configureServer

if [ "$1" = 'start' ]; then
  # Validate if DB_HOST is set.
  checkEnv
  # Validate DB Connection
  checkConnection
  # Configure server
  su craft -c "bash -c configureServer"
  su craft -c "node dist/main.js"
fi

exec runuser -u craft "$@"
