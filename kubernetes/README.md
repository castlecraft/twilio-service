# Create Resources

```shell
cat kubernetes/resources.yaml |
  sed "s|{TWILIO_NUMBER}|+12345678901|;
    s|{TWILIO_SID}|AC420|;
    s|{B64_TWILIO_AUTH_TOKEN}|Q0JEVEhDNzEw|;" |
  kubectl -n fxr-one-staging apply -f -
```
